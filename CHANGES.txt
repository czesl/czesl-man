2021-02-03
- annotation2 retagged, *.ec files for annotation2 added 
- minor fix in MAL_M7_001.b.xml
- added flat versions of annotation1 and annotation2, with T1 and T2 annotation projected on T0,
based on the SkE learner corpus format

2021-02-02
- all pesky files in annotation1 fixed, together with several others
- all texts in annotation1 retagged (linguistic tags, lemmas, errors tags)
- in all IDs and ID references in annotation2 -d1 changed to -d2
(meant to suggest annotation2)
- added *.ec files with comments on manual annotation from the
post-processing script

2021-01-07
Edges in HRD_1M_320 simplified (old > new format)

2020-12-19
A part of MAL_JW_011 was not annotated, fixed

2020-12-19
Several files were not POS tagged, fixed now

2020-12-17

1. Annotated
19 texts without annotation in annotation1:
 
PEC_KC_002.b.xml
PEC_KC_001.b.xml
PEC_3L_006.b.xml
PEC_3L_005.b.xml
NEM_NI_009.b.xml
MAL_KC_001.b.xml
MAL_JW_011.b.xml
MAL_JW_010.b.xml
MAL_JW_009.b.xml
MAL_JW_008.b.xml
KKOL_MS_008.b.xml
KKOL_A4_008.b.xml
KKOL_A3_001.b.xml
KAR_V1_003.b.xml
KAR_NP_007.b.xml
KAR_DO_018.b.xml
HRD_1I_243.b.xml
AA_KK_002.b.xml
AA_KK_001.b.xml

2. Minor errors fixed in 7 texts in annotation1:

HRD_IA_035.b.xml
HRD_C1_092.b.xml
HRD_1Q_177.b.xml
TOU_E505_160.b.xml
HRD_IE_110.a.xml
HRD_AS_057.a.xml
AA_BE_002.b.xml

3. Fixed texts in annotation1-errors:

- corrected errors in the following texts, these files were moved from annotation1-errors to annotation1:
   HRD_1M_320
   HRD_P4_073
   MAL_M7_001

- HRD_EA_003: one sentence in causes feat to give up opening it, issuing a java exception: "Samoz�ejm�, �e m��e z�stat ve sv�, ale taky se m��e p� zapsat do n�jak� lep��, nebo do t� kter� m� stejn� zam��en� jako vysok� �kola, do kter� se chce p�ihl�sit." The text without that sentence and a fixed paragraph boundary is now in annotation1, the original stays in annotation1-errors

- HRD_IB_025: still in annotation1-errors


2019-09-08
- added a list of document ids


2018-04-02  
- corrected errors in T0 conversion (and propagated errors to T1 and T2)
   HRD_SV_151.w.vert:stejni{čárka
   KKOL_AV_006.w.vert:l{a
   MAS_Z2_010.w.vert:Pijí{škrtnutá
   NEM_NI_006.w.vert:sportovny{tečka
   TOU_E510_165.w.vert:veci{a|
   TOU_MM1_001.w.vert:take{o
   MAL_JW_010 e-m + ail

- e- mail tokenized as e-m+ail
  MAL_JW_010.w.xml
  HRD_UE_203.w.xml
  HRD_RN_202.w.xml
  HRD_LV_206.w.xml
  HRD_EA_200.w.xml

- bullets as   li="true" see _all.li
AA_AP_001.w.xml - cleaned, no a references, left .li file
AA_IK_002.w.xml - cleaned
BRY_A2_001.w.xml - cleaned
HRD_F8_370.w.xml - ditto, ??backup of w missing
HRD_IB_025.w.xml - wrong paragraph boundaries, moved to errors todo
HRD_IE_110.w.xml - OK
HRD_PA_181.w.xml - cleaned
HRD_X2_080.w.xml - cleaned, a/b, dashes
HRD_Z3_384.w.xml - cleaned, no a references,
LSA_MO_001.w.xml - cleaned, no a references,
MAL_LH_008.w.xml - cleaned, a/b, dashes
  
TODO:   
- MAL_JW_010 is not annotated

2017-03-13
- T0 vertical: removed correction token references (error in conversion script)
- T0 vertical: replaced all transcriber alternatives by the preferred variant, i.e. {a|b} replaced by a; { } replaced by empty string (to preserve tokenization), {a} replaced by a??
  In two or three cases, corrected apparent error of transcriber/conversion script, e.g.
    ```
	l{a
    /
    u}zen
    ```
    was converted to `lazen`
- T0 vertical: removed all transcriber comments
- T0 vertical: added morphodita tagged version:
 cat all.w.vert | c:\bin\morphodita-1.9.2-bin\bin-win64\run_tagger.exe --input vertical --output vertical c:\bin\morphodita-models\czech-morfflex-pdt-161115\czech-morfflex-pdt-161115.tagger | sed "s/>\t<.*/>/g" > all.w.m.vert
- T0 vertical: morphologically analyzed by morphodita:
  cat all.w.vert | c:\bin\morphodita-1.9.2-bin\bin-win64\run_morpho_analyze --input vertical --output vertical c:\bin\morphodita-models\czech-morfflex-pdt-161115\czech-morfflex-pdt-161115.tagger 0 | sed "s/>\t<.*/>/g" > all.w.mm.vert
  
  TODO remove tabs from all.w.mm.vert to be loadable by feat-morph
